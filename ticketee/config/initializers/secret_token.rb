# Be sure to restart your server when you modify this file.

# Your secret key is used for verifying the integrity of signed cookies.
# If you change this key, all old signed cookies will become invalid!

# Make sure the secret is at least 30 characters and all random,
# no regular words or you'll be exposed to dictionary attacks.
# You can use `rake secret` to generate a secure secret key.

# Make sure your secret_key_base is kept private
# if you're sharing your code publicly.
Ticketee::Application.config.secret_key_base = '8470e29b1d6ff0db57f443c8de88bc92e2982c9f8b08e3039f19b385adf00333b6744e4b8bc230de124c58630f738791460d19b2cb153fa9b0ebdf9b15be3425'
